
## About Sudoku Puzzle

This application created using **Laravel Framework 5.8.19**.

*This application generates a new sudoku puzzle for the users.*

Do the following configuration before you start using it: 
- After clone the repository into your machine.
- cd sudoku_grid.
- Run **cp .env.example .env**.
- Run **php artisan key:generate**.
- Setup .env file and put your database credentials.
- Run **composer install**.
- Setup you custom V-Host, or simply run **php artisan serve**.
