<?php

namespace App;

class Sudoku
{

    private $_puzzle_grid;

    private function _getEmptyPuzzleGrid()
    {
        /**
         * Generating empty array for the puzzle
         */
        return array_fill(0, 9, array_fill(0, 9, 0));
    }

    public function __construct(array $puzzle_grid = null)
    {
        if (!isset($puzzle_grid)) {
            $this->_puzzle_grid = $this->_getEmptyPuzzleGrid();
        } else {
            $this->_puzzle_grid = $puzzle_grid;
        }
    }

    public function generate()
    {
        /**
         * $this->_draw($this->_getEmptyPuzzleGrid()) => Initialize the puzzle
         */

        $this->_puzzle_grid = $this->_draw($this->_getEmptyPuzzleGrid());
        /**
         * $cells => Array with length 30 contains random numbers between 0 and 80
         */
        $cells = array_rand(range(0, 80), 30);

        $i = 0;
        foreach ($this->_puzzle_grid as &$row) {
            foreach ($row as &$cell) {
                if (!in_array($i++, $cells)) {
                    $cell = null;
                }
            }
        }
        return $this->_puzzle_grid;
    }

    public function drawFinalGrid(){
        $puzzle = $this->_puzzle_grid;
        $grid =  "<table style='border-collapse:collapse;border-spacing:0;border:3px solid #000;'>";

        for($i = 0; $i < 9;$i++){
            if($i == 2 || $i == 5){ $ts = 'style="border-bottom:3px solid #000;"'; }else{ $ts = ''; }
            $grid .= "<tr $ts>";
            for($j = 0; $j < 9; $j++){
                if($j == 2 || $j == 5){ $td = 'border-right:3px solid #000;';}else{ $td = ''; }
                $grid .= "<td style='width:40px;height:40px;text-align:center;border:1px solid #000;font-size: 30px;$td'>".$puzzle[$i][$j]."</td>";
            }
            $grid .= "</tr>";
        }

        $grid .= "</table>";

        echo $grid;
    }


    private function _draw($puzzle_grid)
    {
        while (true) {
            $options = array();
            foreach ($puzzle_grid as $rowIndex => $row) {
                foreach ($row as $columnIndex => $cell) {
                    if (!empty($cell)) {
                        continue;
                    }
                    $permissible = $this->_getValidNumbers($puzzle_grid, $rowIndex, $columnIndex);
                    if (count($permissible) == 0) {
                        return false;
                    }
                    $options[] = array(
                        'rowIndex' => $rowIndex,
                        'columnIndex' => $columnIndex,
                        'permissible' => $permissible
                    );
                }
            }
            if (count($options) == 0) {
                return $puzzle_grid;
            }

            foreach ($options[0]['permissible'] as $value) {
                $tmp = $puzzle_grid;
                $tmp[$options[0]['rowIndex']][$options[0]['columnIndex']] = $value;
                if ($result = $this->_draw($tmp)) {
                    return $result;
                }
            }

            return false;
        }
    }

    private function _getValidNumbers($puzzle_grid, $rowIndex, $columnIndex)
    {
        $valid = range(1, 9);
        $invalid = $puzzle_grid[$rowIndex];
        for ($i = 0; $i < 9; $i++) {
            $invalid[] = $puzzle_grid[$i][$columnIndex];
        }
        $box_row = $rowIndex % 3 == 0 ? $rowIndex : $rowIndex - $rowIndex % 3;
        $box_col = $columnIndex % 3 == 0 ? $columnIndex : $columnIndex - $columnIndex % 3;
        $invalid = array_unique(array_merge(
            $invalid,
            array_slice($puzzle_grid[$box_row], $box_col, 3),
            array_slice($puzzle_grid[$box_row + 1], $box_col, 3),
            array_slice($puzzle_grid[$box_row + 2], $box_col, 3)
        ));
        $valid = array_diff($valid, $invalid);
        shuffle($valid);
        return $valid;
    }

}
