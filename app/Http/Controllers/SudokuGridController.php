<?php

namespace App\Http\Controllers;

use App\Sudoku;
use Illuminate\Http\Request;

class SudokuGridController extends Controller
{
    public function index(){
        $puzzle = new Sudoku();
        $puzzle->generate();
        return view('sudoku/index', compact('puzzle'));

    }
}
